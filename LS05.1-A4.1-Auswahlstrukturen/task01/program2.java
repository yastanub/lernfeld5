import java.util.Scanner;

public class program2 {
    public static void main(String[] argv){
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter first number: ");
        int num1 = reader.nextInt();
        System.out.print("Enter second number: ");
        int num2 = reader.nextInt();
        System.out.print("Enter third number: ");
        int num3 = reader.nextInt();

        if (num1 > num2 && num1 > num3) System.out.println("Number one is the biggest");
        if (num3 > num1 || num3 > num2) System.out.println("Number three is bigger than number one or number two");
        int biggestNumber;
        if (num3 > num1 && num3 > num2) biggestNumber = num3;
        else if (num2 > num1) biggestNumber = num2;
        else biggestNumber = num1;

        System.out.printf("Biggest number: %d\n", biggestNumber);
    }
}
