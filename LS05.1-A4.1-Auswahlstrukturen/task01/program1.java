import java.util.Scanner;

public class program1 {
    public static void main(String[] argv){
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter first number: ");
        int num1 = reader.nextInt();
        System.out.print("Enter second number: ");
        int num2 = reader.nextInt();

        if (num1 == num2) System.out.println("number one and number two are the same");
        else if (num1 < num2) System.out.println("number one is smaller than number two");
        else System.out.println("number one is bigger than number two");
    }
}
