import java.util.Scanner;

public class program {
    public static void main(String[] argv){
        Scanner reader = new Scanner(System.in);

        System.out.print("Enter net value: ");
        double netValue = reader.nextDouble();
        String choice;
        do {
            System.out.print("Discounted rate? j/n: ");
            choice = reader.nextLine();
        } while(!choice.equals("j") && !choice.equals("n"));

        double taxRate;
        if (choice.equals("j")) taxRate = 0.16;
        else taxRate = 0.19;

        System.out.printf("Amount to pay: %.2f\n", (1.0 + taxRate) * netValue);
    }
}
