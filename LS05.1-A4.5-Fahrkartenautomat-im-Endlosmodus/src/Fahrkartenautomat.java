import java.util.Scanner;

class TicketPrices{
    public static double single = 2.90;
    public static double dayTicket = 8.60;
    public static double groupTicket = 23.50;
}

class Fahrkartenautomat
{
    public static int GetNumberOfTickets(){
        Scanner input = new Scanner(System.in);
        //variable for the number of tickets to print
        int numTickets = 0;
        //only accept inputs above 0
        while(numTickets <= 0) {
            System.out.print("Wie viele Fahrkarten: ");
            numTickets = input.nextInt();
        }
        //multiply single ticket price with ticket amount
        return numTickets;
    }

    public static double GetPricePerTicket() throws Exception {
        Scanner input = new Scanner(System.in);

        System.out.printf("Single ticket %.2f € (1)\n", TicketPrices.single);
        System.out.printf("Day ticket %.2f € (2)\n", TicketPrices.dayTicket);
        System.out.printf("Group ticket %.2f € (3)\n", TicketPrices.groupTicket);
        System.out.print("Which ticket do you want: ");

        int choice = 0;
        while (choice > 3 || choice < 1){
            choice = input.nextInt();
            if (choice > 3 || choice < 1)
                System.out.print("Unknown selection please try again: ");
        }

        return switch (choice) {
            case 1 -> TicketPrices.single;
            case 2 -> TicketPrices.dayTicket;
            case 3 -> TicketPrices.groupTicket;
            default -> throw new Exception("Unexpected choice in ticket selection");
        };
    }

    public static double ProcessPayment(final double subtotal){
        Scanner input = new Scanner(System.in);
        double coinValue;

        double totalPaid = 0.0;
        while(totalPaid < subtotal)
        {
            System.out.println("Noch zu zahlen: " + (subtotal - totalPaid));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            coinValue = input.nextDouble();
            totalPaid += coinValue;
        }

        return totalPaid - subtotal;
    }

    public static void ProcessChangeIfNecessary(final double overpay) {
        double change = overpay;
        if(change > 0.0)
        {
            System.out.println("Der Rückgabebetrag in Höhe von " + change + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(change >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                change -= 2.0;
            }
            while(change >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                change -= 1.0;
            }
            while(change >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                change -= 0.5;
            }
            while(change >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                change -= 0.2;
            }
            while(change >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                change -= 0.1;
            }
            while(change >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                change -= 0.05;
            }
        }
    }

    public static void PrintTickets(final int numTickets){
        for (int i = 0; i < numTickets; i++) {
            System.out.printf("\nFahrschein %d wird ausgegeben", i + 1);
            for (int j = 0; j < 8; j++) {
                System.out.print("=");
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("\n\n");
        }
    }

    public static void Farewell(){
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }

    public static void main(String[] args) throws Exception
    {
        for (;;) {

            //Get the subtotal
            double pricePerTicket;
            try {
                pricePerTicket = GetPricePerTicket();
            } catch (Exception e) {
                throw new Exception("Could not determine ticket price", e);
            }
            int numTickets = GetNumberOfTickets();
            double subtotal = pricePerTicket * numTickets;

            // Geldeinwurf
            // -----------
            double change = ProcessPayment(subtotal);

            // Fahrscheinausgabe
            // -----------------
            PrintTickets(numTickets);

            // Rückgeldberechnung und -Ausgabe
            // -------------------------------
            ProcessChangeIfNecessary(change);

            Farewell();
        }
    }
}