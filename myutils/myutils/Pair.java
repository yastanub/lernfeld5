package myutils;

public class Pair<L, R> {
    private L lvalue;
    private R rvalue;
    public Pair(L left_value, R right_value){
        lvalue = left_value;
        rvalue = right_value;
    }

    public L getLeft() {
        return lvalue;
    }

    public R getRight() {
        return rvalue;
    }

    public void setLeft(L value){
        lvalue = value;
    }

    public void setRight(R value){
        rvalue = value;
    }
}
