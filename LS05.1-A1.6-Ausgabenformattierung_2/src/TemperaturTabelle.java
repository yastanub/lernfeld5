import java.util.ArrayList;
import java.util.List;

public class TemperaturTabelle {
    public static void main(String[] args) {
        List<Pair<Integer, Double>> values = new ArrayList<>();
        values.add(new Pair<>(-20, -28.8889));
        values.add(new Pair<>(-10, -23.3333));
        values.add(new Pair<>(0, -17.7778));
        values.add(new Pair<>(20, -6.6667));
        values.add(new Pair<>(30, -1.1111));

        System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
        System.out.printf("%s\n", "-".repeat(23));
        for (Pair<Integer, Double> entry: values){
            System.out.printf("%+-12d|%+10.2f\n", entry.getLeft(), entry.getRight());
        }
    }
}
